import pandas as pd
import numpy as np

from sklearn import preprocessing
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline

import category_encoders as ce
from lightgbm import LGBMClassifier

from flask import Flask, Response, request
from joblib import load


app = Flask(__name__)
debug = print
preprocessor = load("preprocessor.joblib")
classifier = load("classifier.joblib")

@app.route('/predict')
def hello_world():
    data = {
        'date': [request.args.get('date')],
        'numeric0': [int(request.args.get('numeric0')) or np.nan],
        'numeric1': [int(request.args.get('numeric1'))],
        'categorical0': [request.args.get('categorical0')],
        'time': [request.args.get('time')],
    }
    debug("data = ", data)
    df = pd.DataFrame(data=data)
    date_series = pd.to_datetime(df.date)
    time_series = pd.to_datetime(df.time)
    df = df.assign(
        year=date_series.dt.year,
        month=date_series.dt.month,
        day=date_series.dt.day,
        n_days=(date_series - date_series.min()) / np.timedelta64(1, 'D'),
        hour=time_series.dt.hour,
        minute=time_series.dt.minute,
        second=time_series.dt.second,
        n_seconds=(time_series - time_series.min()) / np.timedelta64(1, 's')
    )
    pipe = Pipeline(steps=[
        ('preprocessor', preprocessor),
        ('classifier', classifier),
    ])
    X = df.drop(['date', 'time'], axis=1)
    # debug('features:', X.values)
    y_pred = pipe.predict(X)
    # debug('predictions:', y_pred)
    return Response('{"prediction": ' + str(y_pred[0]) + '}', mimetype='application/json')


app.run(host='0.0.0.0', debug=False)
