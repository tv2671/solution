category-encoders==2.2.2
Flask==1.1.2
joblib==0.16.0
lightgbm==3.0.0
numpy==1.19.2
pandas==1.1.2
scikit-learn==0.23.1
