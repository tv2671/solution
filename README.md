"Is it zero or one" guessing API
================================

### Quick Start

    $ docker build -t api:v1 .
    $ docker run -i -t -p 5000:5000 api:v1
    $ curl http://localhost:5000/predict?date=2006-09-25&numeric0=3674&numeric1=1&categorical0=a&time=08:21:33

### Analysis

Data with features like in file `machine_learning_engineer_assignment_data.csv`
(not included, reader is advised to provide it) can be used to learn a model
that uses the other features to estimate what the value of the `target` feature
is.

One possible solution to this learning problem is presented in `Analysis
Notebook.ipynb`. There is a chance the file will open using Jupyter Notebook
instance on one's computer, if appropriate dependended upon libraries are
available and installed. If not, the accompanying `Analysis Notebook.pdf`
contains the results as rendered at some point.

An auxiliary outcome of the notebook are these three files:

 - `preprocessor.joblib` contains preprocessing steps
 - `classifier.joblib` contains the learned model
 - `stats.json` contains statistics about the model performance (F1 score)

As a point of improvement, the repeatibility of running the notebook could be
improved by using an external service, or at least indicating required
dependencies and the versions used. (In addition random seeds could be fixed.)

### Application

The preprocessing and model inference steps are manually copied from the
notebook to `api.py` implementation. This API accepts the same features, in the
following HTTP request parameter format: 

    `http://localhost:5000/predict?date=date&numeric0=numeric0&numeric1=numeric1&categorical0=categorical0&time=time`

And returns the zero or one value as inferred by the model as a JSON response,
for example:

    `{"prediction": 0}`

This is a bit error prone, as the inputs are not checked in any way by the
service. Preprocessing steps perform some checks on the provided values.
Detecting and reporting of errors could be improved dramatically with very
little effort.

### Deployment

The joblib files produced by the notebook have been manually uploaded to a third
party service https://file.io . They are retrieved from there and included in a
docker container image together with reasonably specified dependencies and the
API implementation. As such the image should be deployable to public or private
clouds, but this has not been tested.

In a more realistic scenario this API would be exposed only to the extent
necessary: perhaps not to general public, or if so, with improvements to
erroneous inputs handling and other security aspects like providing SSL
termination and traffic control on top.

### Maintenance and updates

Should further analysis indicate a better model, or newer data prompt a wish to
relearn it, the general outline of updating the implementation would be:

  1. Compare statistics to validate the new model to be better
  2. Update joblib models
  3. Rebuild docker image

It is easy to see that improvements related to preprocessing or feature
engineering may require implementation changes as well. It might be beneficial
to place those changes to a separate file, which could be then imported to the
notebook for further work.
