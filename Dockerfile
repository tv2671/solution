FROM python:3.8-buster
MAINTAINER taneli.vahakangas@welho.com

# Files management, heaviest first
COPY requirements.txt /
RUN pip install -r /requirements.txt
RUN curl -L -o /classifier.joblib https://file.io/wdiQztj9JGxa
RUN curl -L -o /preprocessor.joblib https://file.io/gw61cxycIYsO
WORKDIR /
COPY api.py /

# Configuration
EXPOSE 5000
ENTRYPOINT python /api.py
